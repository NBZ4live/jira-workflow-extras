package com.mailrugames.jira.plugins.jira.workflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.InvalidInputException;
import java.util.Map;

public class CommentRequiredValidator implements Validator
{
    private static final Logger log = LoggerFactory.getLogger(CommentRequiredValidator.class);
    public static final String FIELD_WORD="word";

    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException
    {
    	if (transientVars.get("comment") == null)
    	{
    		throw new InvalidInputException("You must provide a Comment");
    	}
    }
}
